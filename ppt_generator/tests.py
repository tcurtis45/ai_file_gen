from django.urls import reverse
from django.test import Client, TestCase
import logging
import os
import pathlib
from tabulate import tabulate

logger = logging.getLogger(__name__)

class TestViews(TestCase):


    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.output_path = "test_presentation.pptx"
        cls.trackTests = []

    def setUp(self):
        self.client = Client()

    def test_generate_markdown(self):
        logger.info("-----------------------------------------------")
        logger.info(f"TEST RUNNER - {self._testMethodName} - STARTED")
        response = self.client.post(reverse('gpt_input_prompt'), {
            'userInput': 'Test input',
            'gpt_model': 'gpt-4',
        })
        self.assertEqual(response.status_code, 200)
        status = ""
        if response.status_code == 200:
            status = "SUCCESS"
        else:
            status = "FAILED"

        self.trackTests.append({
            "testName": self._testMethodName,
            "status": status,
        })

    def test_enhance_via_gpt(self):
        logger.info("-----------------------------------------------")
        logger.info(f"TEST RUNNER - {self._testMethodName} - STARTED")
        response = self.client.post(reverse('gpt_enhance'), {
            'markup': 'Test markup',
            'gpt_model': 'gpt-4',
        })

        self.assertEqual(response.status_code, 200)
        status = ""

        if response.status_code == 200:
            status = "SUCCESS"
        else:
            status = "FAILED"

        self.trackTests.append({
            "testName": self._testMethodName,
            "status": status,
        })

    def test_save_file(self):
        logger.info("-----------------------------------------------")
        logger.info(f"TEST RUNNERRRRRRR - {self._testMethodName} - STARTED")
        markdown_sample = '''
        # Introduction to Network Automation
        test paragraph
        - Understanding the basics of network automation
        - The importance of network automation in modern networks
        - Key benefits: efficiency, accuracy, and scalability
        <!-- Test notes for main page -->
        ## Ansible
        - Overview of Ansible
        - Key features and advantages
        - Ansible Playbooks and Roles
        <!-- Test notes for Ansible slide -->
        ## Python
        - Why Python for network automation?
        - Common libraries: Netmiko, Paramiko, NAPALM
        <!-- Test notes for Python slide -->
        ## Identifying Automation Opportunities
        - Analyzing repetitive tasks
        - Prioritizing tasks for automation
        - Planning and executing automation projects
        '''
        markup = markdown_sample
        gpt_model = "gpt-4"
        outputPath = self.output_path
        templateName = "ppt_generator/index.html"
        defaultFilename = "test_preso_default.txt"
        logger.info(f"""
        module: {self._testMethodName}\n
        templateName: {templateName}\n
        gpt_model: {gpt_model}\n
        outputPath: {outputPath}\n
        defaultFilename: {defaultFilename}
        """)
        response = self.client.post(reverse('save_file'), {
            'markup': markup,
            'gpt_model': gpt_model,
            'outputPath': outputPath,
            'templateName': templateName,
            'defaultFilename': defaultFilename,
            'is_test_mode': True,
            #'output_path': self.output_path,
        })
        self.assertEqual(response.status_code, 200)
        self.assertGreater(len(response.content), 0, "File is empty")

        status = ""

        if response.status_code == 200:
            if os.path.exists(self.output_path) and os.path.getsize(self.output_path) > 0:
                status = "SUCCESS"
            else:
                status = "FAILED"
        else:
            status = "FAILED"

        self.trackTests.append({
            "testName": self._testMethodName,
            "status": status,
        })

    def tearDown(self):
        logger.info("-----------------------------------------------")
        logger.info(f"TEST RUNNER - {self._testMethodName} - STARTED")
        logger.info(f"Test Method Name - {self._testMethodName}")
        # If the test method that just ran was 'test_save_file', clean up generated test files
        status = ""
        if self._testMethodName == 'test_save_file':
            pathlib.Path('test_presentation.pptx').unlink(missing_ok=True)
            logger.info(f"Test - Teardown File({self.output_path}) for {self._testMethodName} - SUCCESS")
            status = "SUCCESS"
        else:
            logger.info(f"Test - Teardown Files({self.output_path}) for tearDown - NOT EXECUTED")
            status = "N/A"

        self.trackTests.append({
            "testName": "tearDown",
            "status": status,
        })

    @classmethod
    def tearDownClass(cls):
        # prepare a table from the test results
        table = [["Test Name", "Status"]]
        for test in cls.trackTests:
            row = [test['testName'], test['status']]
            table.append(row)

        # print the table
        print("\nTest Summary:")
        print(tabulate(table, headers="firstrow", tablefmt="pipe"))


