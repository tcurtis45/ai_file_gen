from django.shortcuts import render
from django.http import HttpResponse
from .utils import generate_pptx as gpptx
from .forms import pptx_markdown
import logging
import os
from time import sleep

logger = logging.getLogger(__name__)


def index(request):
    logger.info("Rendering ppt_generator/index.html.")
    form = pptx_markdown()
    # View for the index page of the PowerPoint Generator app
    return render(request, 'ppt_generator/index.html', {'form': form})


def save_presentation_old(request):
    logger.info("Running module save_presentation from ppt_generator/views.py.")
    form = pptx_markdown()
    if request.method == 'POST':
        markdown = request.POST.get('markup')
        filename = request.POST.get('outputPath')
        default_filename = request.POST.get('defaultFilename')
        pptx_content, new_file = gpptx.save_file(markdown, filename)  # Generate PPTX from markup
        # Create a response with the PPTX file
        response = HttpResponse(pptx_content.getvalue(), content_type='application/vnd.openxmlformats-officedocument.presentationml.presentation')
        response['Content-Disposition'] = f'attachment; filename="{new_file}"'
        return response
    return render(request, 'ppt_generator/index.html', {'form': form})


def save_file(request, is_test_mode=False):
    logger.info("Running module save_file from ppt_generator/views.py.")
    # mapping for the files, each file type will have corresponding content type and format parameter for save_presentation
    file_types = {
        '.pptx': ('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'bytes'),
        '.ipynb': ('application/x-ipynb+json', None),
        '.txt': ('text/plain', None),  # MIME type for text files
        # add more file types if necessary
    }

    template_name = request.POST.get('templateName')
    form = pptx_markdown()
    if request.method == 'POST':
        markdown = request.POST.get('markup')
        filename = request.POST.get('outputPath')
        default_filename = request.POST.get('defaultFilename', 'default.txt')
        print("-------- POST - save_file - POST --------")
        logger.info(f"outputPath: {filename}")
        logger.info(f"DefaultFilename: {default_filename}")
        logger.info(f"templateName: {template_name}")
        test_mode = request.POST.get('is_test_mode', False)
        logger.info(f"is_test_mode: {test_mode}")
        if test_mode:
            logger.info(f"Django testing mode is enabled.  File will be saved locally.")
        #logger.info(f"markup: {markdown}")
        if filename is None:
            logger.info(f"Filename: NOT SPECIFIED \nDefault Filename: {default_filename}")
            logger.info(f"\n\nUsing default filename: {default_filename}")
            # Updating filename to default filename
            filename = default_filename
        logger.info(f"Filename: {filename} \nDefault Filename: {default_filename}")

        # using os.path.splitext to get file extension
        file_extension = os.path.splitext(filename)[1]
        if file_extension in file_types:
            content_type, file_format = file_types[file_extension]
            logger.info(f"save_file found document type {format(file_format)}")
            file_content, new_file = gpptx.save_file(markdown, filename, file_format, default_filename=default_filename,
                                                     is_test_mode=test_mode)
            # Create a response with the file or if test mode, save file locally.
            if test_mode:
                # Save the file to a local directory
                logger.info(f"File being saved locally: {filename}")
                with open(filename, 'wb') as f:
                    f.write(file_content.getvalue())
                return HttpResponse(f"File saved to {filename}")
            else:
                # Existing logic to return an HTTP response
                logger.info(f"File being returned via requests: {filename}")
                response = HttpResponse(file_content.getvalue(), content_type=content_type)
                response['Content-Disposition'] = f'attachment; filename="{new_file}"'
                return response

    return render(request, template_name, {'form': form})
