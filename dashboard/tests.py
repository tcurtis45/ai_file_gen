from django.test import TestCase, Client
import logging
from api_utils.utils.generic_utils import return_function_name

logger = logging.getLogger(__name__)


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()

    def test_homepage_available(self):
        logger.info(f"TEST RUNNER - {self._testMethodName} - STARTED")
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        logger.info(f'TEST RUNNER - {self._testMethodName} - PASSED')

    def test_jupyter_notes_available(self):
        logger.info(f"TEST RUNNER - {self._testMethodName} - STARTED")
        response = self.client.get('/jupyter_notes/')
        self.assertEqual(response.status_code, 200)
        logger.info(f'TEST RUNNER - {self._testMethodName} - PASSED')
        
    def test_ppt_generator_available(self):
        logger.info(f"TEST RUNNER - {self._testMethodName} - STARTED")
        response = self.client.get('/ppt_generator/')
        self.assertEqual(response.status_code, 200)
        logger.info(f'TEST RUNNER - {self._testMethodName} - PASSED')
