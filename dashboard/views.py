from django.shortcuts import render
import logging

logger = logging.getLogger(__name__)

def index(request):
    # View for the index page of the PowerPoint Generator app
    logger.info("Rendering Homepage - dashboard/index.html")
    return render(request, 'dashboard/index.html')

