import functools


# Wrapper function to get the name of the functions and return as an argument.
def return_function_name(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return func.__name__
    return wrapper

