from django.apps import AppConfig


class JupyterNotesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'jupyter_notes'
