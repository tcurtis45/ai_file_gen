from django.shortcuts import render
from django.http import HttpResponse
import logging

logger = logging.getLogger(__name__)


def index(request):
    logger.info("Rendering jupyter_notes/index.html.")
    return render(request, 'jupyter_notes/index.html')

# def index(request):
#     return render(request, 'jupyter_notes/index.html')
